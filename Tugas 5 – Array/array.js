/////////////////////////soal-satu////////////////////////
console.log("===================soal-satu================");
function range(startNum, finishNum) {
    //membuat variabel lokal array
    let array = [];
    //melakukan pengecekan parameter
    if(startNum < finishNum){
        for (let i = startNum; i <= finishNum; i++) {
            // menmbah index array 
            array.push(i);
        }
    } else if(startNum > finishNum){
        for (let i = startNum; i >= finishNum; i--) {
            array.push(i);
        }
    } else if( startNum === 1 && finishNum === undefined){
        array.push(-1);
    } else{
        array.push(-1);
    }
    return array;
}
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

/////////////////////////soal-dua////////////////////////
console.log("===================soal-dua================");
function rangeWithStep(startNum, finishNum, step) {
    let array = [];
    if(startNum < finishNum){
        for (let i = startNum; i <= finishNum; i +=step) {
            // menmbah index array 
            array.push(i);
        }
    } else{
        for (let i = startNum; i >= finishNum; i -=step) {
            array.push(i);
        }
    }
    return array;    
}
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5,2,1));
console.log(rangeWithStep(29,2,4));

/////////////////////////soal-tiga////////////////////////
console.log("===================soal-tiga================");
function sum(startNum, finishNum,step) {
    var jumlah = 0;
    if(startNum < finishNum){
        for (let i = startNum; i <= finishNum; i += step) {
            jumlah += i ;
        }
    } else if(startNum > finishNum){
        for (let i = startNum; i >= finishNum; i -= step) {
            jumlah += i ;
        }
    } else if( startNum === 1 && finishNum === undefined){
        jumlah += 1 ;
    } else{
        jumlah += 0 ;
    }
    return jumlah;
}
console.log(sum(1,10,1));
console.log(sum(5, 50, 2));
console.log(sum(15,10,1));
console.log(sum(20, 10, 2));
console.log(sum(1,0,1));
console.log(sum(0,0,0));

/////////////////////////soal-empat////////////////////////
console.log("===================soal-empat================");
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(){
    for(var i = 0 ; i <= input.length -1 ; i++){
        for(var j = 0 ; j <= input.length-1; j++){
            console.log("nod: "+ [i][0])
        }
        console.log("nod: "+ [0][j])
    }
}
dataHandling();