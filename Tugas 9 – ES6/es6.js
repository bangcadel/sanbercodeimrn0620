/////////////////////soal-satu////////////////////////////
const golden = () =>{
    console.log('this is golden');
}
golden();

/////////////////////soal-dua////////////////////////////
const newFunction = (firstName,lastName) => {
    return {
        firstName,
        lastName,
        fullName: ()=>{
            console.log(firstName + " " + lastName)
            return;
        }
    }
}
newFunction("William", "Imoh").fullName()

/////////////////////soal-tiga////////////////////////////
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const {firstName, lastName, destination, occupation,spell} = newObject;
console.log(firstName, lastName, destination, occupation,spell);

/////////////////////soal-empat////////////////////////////
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
console.log(combined);

/////////////////////soal-lima////////////////////////////
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before);

