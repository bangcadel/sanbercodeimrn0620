//////////////////////soal-satu///////////////
function arrayToObject(array){
    var object = {};
    //object waktu
    var d = new Date();
    var now = d.getFullYear();
    var age = "";

    for(let i = 0 ; i < array.length; i++ ){
        for(let j = 0; j < array.length; j++){
            if (array[i][3] > now || array[i][3] == undefined) {
                age = "Invalid birth year";
            } else {
                age = now - array[i][3];
            }
        }
        object.firstName = array[i][0];
        object.lastName = array[i][1];
        object.gender = array[i][2];
        object.age = age;
        console.log(object);
    }   
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2);

////////////////////soal-dua///////////////
var barang = [["Sepatu brand Stacattu", 1500000], ["Sweater brand Uniklooh", 175000],["Baju brand Zoro", 500000], 
        ["Baju brand H&N", 250000], ["Casing Handphone", 50000]];
    //mengurutkan array
    barang = barang.sort(function(a, b) {return b[1] - a[1]});

function shoppingTime(memberId, money) {
    var obeject2 = {};
    var dapatBarang = [];
    var changeMoney = 0;

    if (memberId != '' && money != null) {
        if (money >= 50000) {
            changeMoney = money;
            for (let i = 0; i < barang.length; i++) {
                if (changeMoney >= barang[i][1]) {
                    changeMoney -= barang[i][1]
                    dapatBarang.push(barang[i][0])
                }
            }
            obeject2.memberId = memberId;
            obeject2.money = money;
            obeject2.listPurchased = dapatBarang;
            obeject2.changeMoney = changeMoney; 
            return obeject2
        }else {
           return 'Mohon maaf, uang tidak cukup';
        }      
    }else {
        return 'Mohon maaf, toko i hanya berlaku untuk member saja'
    }
    
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRia53', 15000));
console.log(shoppingTime());

//////////////////soal-tiga///////////////
function naikAngkot(penumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    var array = [];
    var naik = 0;
    var tujuan = 0;
    var naik = 0;
    var turun = 0;
    var bayar = 0;

    if (penumpang !== undefined || penumpang.length != 0) {
        for (let i = 0; i < penumpang.length; i++) {
            naik = penumpang[i][1];
            tujuan = penumpang[i][2];
    
            for (let index = 0; index < rute.length; index++) {
                if (naik == rute[index]) {
                    naik = index + 1;
                }
                if (tujuan == rute[index]) {
                    turun = index + 1;
                }
            }
            var object3 = {};
            bayar = (turun - naik) * 2000;
            object3.penumpang = penumpang[i][0];
            object3.naikDari = naik;
            object3.tujuan = tujuan;
            object3.bayar = bayar;
            array.push(object3);
        }
    }
    return array;
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));