/////////////////soal-satu///////////////
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// link : https://www.javascripttutorial.net/javascript-callback/
readBooks(1000,books[0],function(sisaWaktu){
	readBooks(sisaWaktu,books[1],function(sisaWaktu){
		readBooks(sisaWaktu,books[2],function(sisaWaktu){
		});
	});
});
