//////////////////////////// soal-satu /////////////////
function teraik(){
    return "Halo Sanbers!";
}
console.log(teraik());



//////////////////////////// soal-dua /////////////////
function kalikan(){
    return num1*num2;
}
var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1,num2)
console.log(hasilKali);



console.log('============ cara lain ===================');
function kalikanSatu(bil1,bil2){
    var kali = bil1*bil2;
    return kali;
}
var hasil = kalikanSatu(12,4);
console.log(hasil);



//////////////////////////// soal-tiga /////////////////
function introduce(){
    return "Nama saya " + name +  ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby ;
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);

console.log('============ cara lain ===================');
function introduceSatu(name, age, address, hobby){
    var perkenalan = "Nama saya " + name +  ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby ;
    return perkenalan;
}
var sapa = introduceSatu("Agus",30,"Jln. Malioboro, Yogyakarta","Gaming");
console.log(sapa);