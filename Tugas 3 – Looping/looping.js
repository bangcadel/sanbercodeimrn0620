///////////////////////////////soal-satu////////////////////////////////
console.log('LOOPING PERTAMA');
var bilangan = 2;
while(bilangan <= 20){
    console.log(bilangan + " " + "-" + " " + "I love coding");
    bilangan +=2;
}
console.log('LOOPING KEDUA');
var bil = 20;
while(bil >= 2){
    console.log(bil + " " + "-" + " " + "I will become a mobile developer");
    bil -=2;
}

///////////////////////////////soal-dua////////////////////////////////
// memulai dengan nilai 1 
for(var nomor = 1; nomor <= 20 ; nomor++){
    // kondisi true membuat looping di mulai dari nomor 1
    // pada saat looping nomor 1 di perlukan untuk mencek 
    //apakah nomor 1 bilangan kelipatan 3 atau ganjil atau genap

    // membuat control flow untuk cek nomor
    // yang menjadi if pertama adalah if dengan cek nomor
    // apakah kelipatan 3 dan ganjil
    // karna jika if cek nomor kelipatan 3 dan ganjil
    //diletakkan paling atas tidak akan di eksekusi karna kondisi true dari
    //if sebelum nya ke bawah setelah nomor genap
    //maka console.log dari nomor kelipatan 3 dan ganjil
    // tidak akan muncul
    if(nomor%3 === 0 && nomor%2 === 1){ 
        console.log(nomor + " " + "-" + " " + 'I Love Coding');
    } else if(nomor %2 == 1){
        console.log(nomor + " " + "-" + " " + 'Santai');
    }else if(nomor%2 == 0){
        console.log(nomor + " " + "-" + " " + 'Berkualitas');
    }
}



///////////////////////////////soal-tiga////////////////////////////////
/// membuat variabel gabung dengan tipe data string agar bisa digabungkan dengan #
// dan menampilkan variabel gabung di akhir
var gabung = "";
for(var baris = 0; baris < 7 ; baris ++){ 
    for(var kolom = 0; kolom < 7 ; kolom++){ // jika kondisi ini benar akan membuat 7 kolom 
        gabung = gabung.concat('#') // karna berhasil maka nilai dari variabel gabung di ubah dengan menggabungkan '#'
    }
    gabung = gabung.concat('\n'); // selanjutnya setelah selasai looping dari kolom maka nilai dari variabel 
    //gabung juga di ubah dan digabungkan dengan '\n' agar output memiliki baris 
    //karna kalua tidak digabung dengan '\n' hasil nya akan baris
}
console.log(gabung);

//cara lain tampa menggunakan method concat() dan bisa diterapkan untuk soal selanjutnya
console.log("========================== cara lain ==========================")
var pagar = "";
for(var baris = 0; baris < 7; baris ++){
    for(var kolom = 0 ; kolom < 7 ; kolom++){
        pagar += "#";
    }
    pagar += "\n";
}
console.log(pagar);



///////////////////////////////soal-empat////////////////////////////////
var gabungDua = "";
for(var baris = 0; baris < 8 ; baris ++){ 
    for(var kolom = 0; kolom < 8 ; kolom++){ 
        if(baris == kolom){
            break; // fungsi break untuk mengakhiri proses looping
        } 
        gabungDua = gabungDua.concat('#') 
    }
    gabungDua = gabungDua.concat('\n');
}
console.log(gabungDua);



///////////////////////////////soal-lima////////////////////////////////
var gabungTiga = "";
for(var baris = 0; baris < 8 ; baris ++){ // jika kondisi ini benar akan membuat 7 baris
    for(var kolom = 0; kolom < 8 ; kolom++){ // jika kondisi ini benar akan membuat 7 kolom
        if((baris+kolom)% 2 == 0){
            gabungTiga = gabungTiga.concat(" ");
        } else{
            gabungTiga =  gabungTiga.concat("#");
        }     
    }
    gabungTiga = gabungTiga.concat('\n'); 
}
console.log(gabungTiga);