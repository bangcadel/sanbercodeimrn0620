////////////////////////////////////////////////////////////////////////////////Soal-satu///////////////////////////////////////////////////////////////
// membuat variabel
var kataPertama = "JavaScript ";
var kataKedua = "is ";
var kataKetiga = "awesome ";
var kataKeempat = "and ";
var kataKelima = "I ";
var kataKeenam = "love ";
var kataKetujuh = "it!"

// penggabungan string menggunakan fungsi .concat([string])
// ada beberapa variabel yang akan digabungkan maka menambahkan di parameter
console.log(kataPertama.concat(kataKedua,kataKetiga,kataKeempat,kataKelima,kataKeenam,kataKetujuh));
//saya mencoba cara lain seperti, namun sepertinya kurang tepat
console.log(kataPertama.concat(kataKedua).concat(kataKetiga).concat(kataKeempat).concat(kataKelima).concat(kataKeenam).concat(kataKetujuh));
// saya juga mencoba seperti ini
var gabungKata = kataPertama.concat(kataKedua,kataKetiga,kataKeempat,kataKelima,kataKeenam,kataKetujuh);
console.log(gabungKata);

////////////////////////////////////////////////////////////////////////////////Soal-dua///////////////////////////////////////////////////////////////
//membuat varibel
var kalimat = "I am going to be React Native Developer";
// membuat 8 variabel baru
// index string dimulai dari 0
// melakukan pengecekan index
// console.log(kalimat.indexOf("I"))
// console.log(kalimat.indexOf("am"))
// console.log(kalimat.indexOf("going"))
// console.log(kalimat.indexOf("to"))
// console.log(kalimat.indexOf("be"))
// console.log(kalimat.indexOf("React"))
// console.log(kalimat.indexOf("Native"))
// console.log(kalimat.indexOf("Developer"))
var kataSatu = kalimat[0];
var kataDua = kalimat[2] + kalimat[3];
var kataTiga = kalimat[5]+kalimat[6]+kalimat[7]+kalimat[8]+kalimat[9];
var kataEmpat = kalimat[11]+kalimat[12];
var kataLima = kalimat[14]+kalimat[15];
var kataEnam = kalimat[17]+kalimat[18]+kalimat[19]+kalimat[20]+kalimat[21];
var kataTujuh = kalimat[23]+kalimat[24]+kalimat[25]+kalimat[26]+kalimat[27]+kalimat[28];
var kataDelapan = kalimat[30]+kalimat[31]+kalimat[32]+kalimat[33]+kalimat[34]+kalimat[35]+kalimat[36]+kalimat[37]+kalimat[38];

console.log('First Word: ' + kataSatu); 
console.log('Second Word: ' + kataDua); 
console.log('Third Word: ' + kataTiga); 
console.log('Fourth Word: ' + kataEmpat); 
console.log('Fifth Word: ' + kataLima); 
console.log('Sixth Word: ' + kataEnam); 
console.log('Seventh Word: ' + kataTujuh); 
console.log('Eighth Word: ' + kataDelapan);

////////////////////////////////////////////////////////////////////////////////Soal-tiga///////////////////////////////////////////////////////////////
//membuat variabel
var kalimatDua = "wow JavaScript is so cool";
// membuat 5 variabel
var wordSatu = kalimatDua.substr(0,3);
var wordDua = kalimatDua.substr(4,10);
var wordTiga = kalimatDua.substr(15,2);
var wordEmpat = kalimatDua.substr(18,2);
var wordLima = kalimatDua.substr(21,4);

console.log('First Word: ' + wordSatu); 
console.log('Second Word: ' + wordDua); 
console.log('Third Word: ' + wordTiga); 
console.log('Fourth Word: ' + wordEmpat); 
console.log('Fifth Word: ' + wordLima);


////////////////////////////////////////////////////////////////////////////////Soal-empat///////////////////////////////////////////////////////////////
//membuat variabel
var kalimat2 = "wow JavaScript is so cool";
//membuat lima variabel
var wordSatu = kalimatDua.substr(0,3);
var wordDua = kalimatDua.substr(4,10);
var wordTiga = kalimatDua.substr(15,2);
var wordEmpat = kalimatDua.substr(18,2);
var wordLima = kalimatDua.substr(21,4);
var word1 = wordSatu.length;
var word2 = wordDua.length;
var word3 = wordTiga.length;
var word4 = wordEmpat.length;
var word5 = wordLima.length;

console.log('First Word: ' + wordSatu + ', with length: ' + word1);
console.log('Second Word: ' + wordDua + ', with length: ' + word2); 
console.log('Third Word: ' + wordTiga + ', with length: ' + word3);
console.log('Fourth Word: ' + wordEmpat + ', with length: ' + word4);
console.log('Five Word: ' + wordLima + ', with length: ' + word5);
