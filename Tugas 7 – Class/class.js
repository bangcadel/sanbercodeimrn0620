///////////////////////soal-satu///////////
class Animal {
    constructor(name){
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    // method get
    get name(){
        return this._name;
    }
    get legs (){
        return this._legs;
    }
    get cold_blooded(){
        return this._cold_blooded;
    }
}

///////////////////////kelas-frog///////////
class Frog extends Animal{
    constructor(name){
        super(name);  
    }
    jump(){
        console.log('Hop');
    }
}

///////////////////////kelas-frog///////////
class Ape extends Animal{
    constructor(name){
        super(name);
    }
    yell(){
        console.log('Auooo');
    }

    // mengubah metod kaki
    get legs (){
        return 2;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

var kodok = new Frog("buduk");
console.log(kodok.name);
kodok.jump();
console.log(kodok.legs);
console.log(kodok.cold_blooded);

var sungokong = new Ape("kera sakti");
console.log(sungokong.name);
sungokong.yell();
console.log(sungokong.legs);
console.log(sungokong.cold_blooded);


///////////////////////soal-dua///////////////
class Clock {
    constructor({template}) {
        this.template = template;
        this.timer = 0;
    }
    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) {
            hours = '0' + hours;
        }
        var mins = date.getMinutes();
        if (mins < 10) {
            mins = '0' + mins;
        }
        var secs = date.getSeconds();
        if (secs < 10) {
            secs = '0' + secs;
        }
        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        console.log(output)
    }
    stop() {
        clearInterval(this.timer);
    }
    start() {
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();